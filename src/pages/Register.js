// import {useState, useEffect} from 'react';

// import UserContext from '../UserContext';

// import { Form, Button } from 'react-bootstrap';

// import {useContext} from 'react';

// import { Navigate } from 'react-router-dom';

// export default function Register() {

//     // to store and manage value of the input fields
//     const [email, setEmail] = useState("");
//     const [password1, setPassword1] = useState("");
//     const [password2, setPassword2] = useState("");
//     // to determine whether submit button is enabled or not
//     const [isActive, setIsActive] = useState(false);

//      const {unsetUser, setUser} = useContext(UserContext);
//     // localStorage.clear();
//     unsetUser();

//     useEffect(() => {
//         setUser({register:null})
//     });

//     return (
//         <Navigate to="/courses"/>
//     )

//     useEffect(() => {
//         if (( email !== "" && password1 !== "" && password2 !== "") && password1 === password2) {
//             setIsActive(true);
//         } else {
//             setIsActive(false);
//         };
//     }, [email, password1, password2]);

//     // function to simulate user registration
//     function registerUser(e) {
//         e.preventDefault();

//         // Clear input fields
//         setEmail("");
//         setPassword1("");
//         setPassword2("");

//         alert("Thank you for registering!");
//     };

//     return (
//         <Form onSubmit={(e) => registerUser(e)}>
//             <Form.Group controlId="userEmail">
//                 <Form.Label>Email address</Form.Label>
//                 <Form.Control 
//                     type="email" 
//                     placeholder="Enter email" 
//                     value={email}
//                     onChange={e => setEmail(e.target.value)}
//                     required
//                 />
//                 <Form.Text className="text-muted">
//                     We'll never share your email with anyone else.
//                 </Form.Text>
//             </Form.Group>

//             <Form.Group controlId="password1">
//                 <Form.Label>Password</Form.Label>
//                 <Form.Control 
//                     type="password" 
//                     placeholder="Password" 
//                     value={password1}
//                     onChange={e => setPassword1(e.target.value)}
//                     required
//                 />
//             </Form.Group>

//             <Form.Group controlId="password2">
//                 <Form.Label>Verify Password</Form.Label>
//                 <Form.Control 
//                     type="password" 
//                     placeholder="Verify Password"
//                     value={password2}
//                     onChange={e => setPassword2(e.target.value)}
//                     required
//                 />
//             </Form.Group>

//             {isActive ?
//             <Button variant="primary" type="submit" id="submitBtn">
//                 Submit
//             </Button>
//                 :
//             <Button variant="danger" type="submit" id="submitBtn" disabled>
//                 Submit
//             </Button>
//             }
            
//         </Form>
//     )

// }

// S54 activity solution

import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import {Navigate, useNavigate} from 'react-router-dom';

import { Form, Button } from 'react-bootstrap';

import Swal from 'sweetalert2';



export default function Register() {
    const navigate = useNavigate()
    const {user} = useContext(UserContext);

    // to store values of the input fields
    const [firstName, setfirstName] = useState("");
    const [lastName, setlastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setmobileNo] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    const [password, setPassword] = useState("");
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

   
// const checkEmailUser = (courseId) => {
//         fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
//             method: "POST",
//             headers: {
//                 'Content-Type': 'application/json',
//                 Authorization: `Bearer ${localStorage.getItem('token')}`
//             },
//             body: JSON.stringify({
//                 courseId:courseId
//             })
//         })
//         .then(res => res.json())
//         .then(data => {
//             console.log(data)

    //         if(data !=== true) {
    //              Swal.fire({
    //                 title: "Duplicate email found",
    //                 icon: "error",
    //                 text: "Please provide another email"
    //             })

    //             navigate("/register")
    //         }
    //     }
    // }

useEffect(() => {
    if((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "" && mobileNo.length === 11) && (password1 === password2)) {
        setIsActive(true);
    } else {
        setIsActive(false);


    }
}, [email, password1, password2, firstName, lastName, mobileNo, password]);

    // function to simulate user registration
    function registerUser(e) {
        // Prevents page from reloading       
        e.preventDefault();
                fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                 email: email
                
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data !== true) {
                     fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                         method: "POST",
                         headers: {
                        'Content-Type': 'application/json',
                                             },
                             body: JSON.stringify({
                             firstName:firstName,
                             lastName: lastName,
                             email: email,
                             mobileNo: mobileNo,
                             password: password1   
            })
        })
           Swal.fire({
                    title: "Registration Successful!",
                    icon: "success",
                    text: "Welcome to zuitt"
                })

                navigate("/login")

        // .then(res => res.json())
        // .then(data => {
        //     console.log(data)

            } else {
                         Swal.fire({
                    title: "Duplicate email found",
                    icon: "error",
                    text: "Please provide another email"
                })

                navigate("/register")  
            }

        })
 // Clear input fields
        setfirstName("");
        setlastName("");
        setEmail("");
        setmobileNo("");
        setPassword1("");
        setPassword2("");    

    
}

    return (
        (user.id !== null)?
        <Navigate to ="/courses" />
        :
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="firstName" 
                    placeholder="Enter First Name" 
                    value={firstName}
                    onChange={e => setfirstName(e.target.value)}
                    required
                /> 
            </Form.Group>


             <Form.Group controlId="userlastName">
                <Form.Label>Last Name</Form.Label>
               <Form.Control 
                   type="lastName" 
                   placeholder="Enter Last Name" 
                   value={lastName}
                    onChange={e => setlastName(e.target.value)}
                   required
                />
             
            </Form.Group>

 
             <Form.Group controlId="usermobileNo">
                 <Form.Label>Mobile Number</Form.Label>
                 <Form.Control 
                     type="mobileNo" 
                     placeholder="Enter Mobile Number" 
                     value={mobileNo}
                    onChange={e => setmobileNo(e.target.value)}
                     required
                />
             
             </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>password2</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
        </Form>
    )

}