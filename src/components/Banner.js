import { Button, Row, Col } from 'react-bootstrap';
import {useNavigate} from 'react-router-dom'
import {useState, useEffect} from 'react'

export default function Banner({notFound}) {
    const navigate = useNavigate()

    function back(e){
        navigate('/')
    }

return (
     (notFound) ?
    <>
    <Row>
        <Col className="p-5">
            <h1>404 Page not found</h1>
            <p>Opportunities for everyone, everywhere.</p>
            <Button onClick={e=> back(e)}>Back to home</Button>
        </Col>
    </Row>
    </>
    :
    <>
    <Row>
        <Col className="p-5">
            <h1>Zuitt Coding Bootcamp</h1>
            <p>Opportunities for everyone, everywhere.</p>
            <Button variant="primary">Enroll now!</Button>
        </Col>
    </Row>
    </>
    

    )
}
