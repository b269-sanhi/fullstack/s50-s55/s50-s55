import { Button, Row, Col, Card } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useState, useEffect} from 'react'

export default function CourseCard({course}) {

        const {name, description, price, _id } = course;
        // SYNTAX:
        // const [getter, setter] = useState (initialGetterValue)
        // const [count, setCount] = useState(0)
        // const [seats, setSeats] = useState(5)

        // function enroll() {
            // if (seats > 0){
            //     setCount(count + 1)
            //     console.log('Enrollees: ' + count)
            //     setSeats(seats - 1)
            //     console.log('Seats:' + seats)
            // }else {
            //     alert("Noe more seats")
            // }
//             setCount(count + 1)
//             setSeats(seats - 1)
//         }
// useEffect(() => {
//     if (seats <= 0){
//         alert('No more seats')
//     }
// }, [seats])

       

return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}course offering</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
                       {/* <Card.Text>Count: {count}</Card.Text>
                        <Card.Text>Seats: {seats}</Card.Text>*/}
                        {/*<Button variant="primary" onClick={enroll}disabled={seats<=0}>Enroll</Button>*/}
                        <Button className = "bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}





// import Card from 'react-bootstrap/Card';
// import Button from 'react-bootstrap/Button';

// function CourseCard() {
//   return (
//     <Card>
//       <Card.Body>
//                         <Card.Title>
//                             <h2>JavaScript</h2>
//                         </Card.Title>
//                         <Card.Text>
//                            Description: Learn from backend
//                         </Card.Text>
//                          <Card.Text>
//                            This is a sample course offering
//                         </Card.Text>
//                          <Card.Text>
//                           Price:
//                         </Card.Text>
//                          <Card.Text>
//                            PHP 5,000
//                         </Card.Text>
//                            <Button variant="primary">Enroll</Button>
//                     </Card.Body>
//     </Card>
//   );
// }




 